import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-post-list-component',
  templateUrl: './post-list-component.component.html',
  styleUrls: ['./post-list-component.component.css']
})
export class PostListComponentComponent implements OnInit {
@Input() postTitle: string;
@Input() postContent: string;
@Input() postLovesIts: number;
@Input() postCreate: Date;
  constructor() { }

  ngOnInit() {
  }
getLovesIts() {
    return this.postLovesIts;
}

getColor() {
    if (this.postLovesIts > 0) {
      return 'green';
    } else if (this.postLovesIts > 0) {return 'red';
    }
}
addLove() {
    this.postLovesIts++;
}
subtractLove() {
    this.postLovesIts--;
}
}

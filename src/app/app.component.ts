import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  isAuth = false;
  lastUpdate = new Date();
  initLove = 0;
  Post = [
    {title: 'Mon premier post',
    content: 'Nam congue, odio vel dapibus hendrerit, ' +
      'urna magna sodales dui, ac interdum tortor tellus ' +
      'varius lacus. Aliquam et sollicitudin nunc. Vivamus ut ' +
      'est pellentesque, varius mi ut, congue neque. Ut ullamcorpe' +
      'r nulla non elit viverra aliquet. Aliquam hendrerit nisi sem, ' +
      'aliquet feugiat arcu varius sed. Curabitur eu efficitur diam. Nu' +
      'llam pretium nisi nulla, id tempor mauris sodales id. Phasellus dictum eget dolor at rhoncus.',
    loveIts: this.initLove,
    created_at: this.lastUpdate
  },
    {
      title: 'Second post',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc maximus ullamcorper scelerisque. Nunc in accumsan urna. ' +
        'Donec ullamcorper felis vel dui tempor viverra' +
        '. Sed molestie blandit rutrum. Vestibulum ornare arcu elementum, ullamcorper metus et, ' +
        'dignissim neque. Praesent pretium luctus ph' +
        'aretra. Duis hendrerit venenatis quam a malesuada. Proin lorem magna, interdum nec tempus eget' +
        ', volutpat eget nisl.',
      loveIts: this.initLove,
      created_at: this.lastUpdate,
    },
    {
      title: 'Final post',
      content: 'Bonus non compris',
      loveIts: this.initLove,
      created_at: this.lastUpdate,
    }
];


  constructor() {
  setTimeout(
    () => {
      this.isAuth = true;
    }, 4000
  );
  }

}
